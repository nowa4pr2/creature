package at.campus02.nowa.bmi.model;

public abstract class Creature {
	private double weight;
	private double height;
	private String name;
	private int age;
	
	
	public Creature() {
		
	}

	protected int getAge() {
		return  age;
	}
	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public abstract void eat();
	public abstract void trainig();
	
}
