package at.campus02.nowa.bmi;

import at.campus02.nowa.bmi.contoller.CalculateBMI;
import at.campus02.nowa.bmi.model.Dog;
import at.campus02.nowa.bmi.model.Fish;

public class App {

	public static void main(String[] args) {
		
		Dog dog = new Dog();
		dog.setName("pepi");
		dog.setHeight(2.4);
		dog.setWeight(2);
		CalculateBMI cal = new CalculateBMI();
		double dogBMI = cal.calculate(dog.getWeight(), dog.getHeight());
		System.out.println("Dog BMI: " + dogBMI);
		
		Fish fish = new Fish();
		fish.setName("Wand");
		fish.setHeight(0.4);
		fish.setWeight(0.5);
		double fishBMI = cal.calculate(fish.getWeight(), fish.getHeight());
		System.out.println("Fish BMI: " + fishBMI);
	}

}
